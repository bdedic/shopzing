from django import forms


class ContactForm(forms.Form):
    name    = forms.CharField(max_length=80, required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Full Name'}))
    email   = forms.EmailField(required=True, widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Email'}))
    content = forms.CharField(max_length=5000, required=True, widget=forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Your Message'}))
