from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .forms import ContactForm
from products.models import Product
from wishlist.models import WishList
from django.views.generic import ListView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger





# def main_page(request):
#     context = {
#         'title': 'Home',
#         'content': 'Welcome to the home page'
#     }
#     return render(request, 'home_page.html', context)

# class ProductDiscountedListView(ListView):
#     template_name = 'home_page.html'
   
#     def get_queryset(self, *args, **kwargs):
#         request = self.request
#         return Product.objects.all()

class HomePage(ListView):
    model = Product
    template_name='home_page.html'
    context_object_name = 'products'  # Default: object_list
    paginate_by = 4


    def get_context_data(self, **kwargs):
        context = super(HomePage, self).get_context_data(**kwargs)
        request = self.request
        if request.user.is_authenticated:
            wishlist_obj, new_obj = WishList.objects.new_or_get(request)
            context['wishlist'] = wishlist_obj
        return context

    def get_queryset(self, *args, **kwargs):
        queryset = super(HomePage, self).get_queryset()
        queryset = Product.objects.all().discounted()
        paginator = Paginator(queryset, self.paginate_by)
        page = self.request.GET.get('page')
        request = self.request
        if request.user.is_authenticated:
            wishlist_obj, new_obj = WishList.objects.new_or_get(request)
            # wishlist_obj = WishList.objects.all().get(user=request.user)
            if request.user == wishlist_obj.user:
                request.session['wishlist_items'] = wishlist_obj.products.count()
        try:
            products = paginator.page(page)
        except PageNotAnInteger:
            products = paginator.page(1)
        except EmptyPage:
            products = paginator.page(paginator.num_pages)
        
        return queryset
    





def contact_page(request):
    contact_form = ContactForm(request.POST or None)
    context = {
        'title': 'Contact',
        'content': 'Welcome to the contact page',
        'form': contact_form
    }
    if contact_form.is_valid():
        if request.is_ajax():
            return JsonResponse({"message": "Thank you for your submission"})
    if contact_form.errors:
        errors = contact_form.errors.as_json()
        if request.is_ajax():
            return HttpResponse(errors, status=400, content_type='application/json')
        
    return render(request, 'contact/view.html', context)