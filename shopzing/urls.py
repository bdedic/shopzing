"""shopyfy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from .views import contact_page, HomePage
from accounts.views import LoginView, RegisterView
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.urls import include, path
from wishlist.views import wishlist_home, wishlist_detail_api_view
from django.views.generic.base import RedirectView


urlpatterns = [
    path('', HomePage.as_view(), name='home'),
    # path('wishlist/', WishListView.as_view(), name='wishlist'),
    path('accounts/', RedirectView.as_view(url='/account')),
    path('account/', include(('accounts.urls', 'accounts'), namespace = 'account')),
    path('accounts/', include('accounts.passwords.urls')),
    path('contact/', contact_page, name='contact'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
    # path('', include(('accounts.urls', 'accounts'), namespace = 'accounts')),
    # path('', include('accounts.passwords.urls')),
    path('notifications/', include(('notifications.urls', 'notifications'), namespace='notifications')),
    path('api/wishlist/', wishlist_detail_api_view, name='api-wishlist'),
    path('wishlist/', include(('wishlist.urls', 'wishlist'), namespace = 'wishlist')),
    path('', include(('products.urls', 'products'), namespace = 'products')),
    path('search/', include(('search.urls', 'search'), namespace = 'search')),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns = urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
