from django.conf import settings
from django.db import models

from products.models import Product


User = settings.AUTH_USER_MODEL

class WishListManager(models.Manager):
    def new_or_get(self, request):
        #request = self.request
        # wishlist_id = request.session.get('wishlist_id', None)
        # qs = self.get_queryset().filter(id=wishlist_id)
        qs = self.get_queryset().filter(user=request.user)
        if qs.count() == 1:
            new_obj = False
            wishlist_obj = qs.first()
            if request.user.is_authenticated and wishlist_obj.user is None:
                wishlist_obj.user = request.user
                wishlist_obj.save()
        else:
            # wishlist_obj = self.new_wishlist(user=request.user)
            wishlist_obj = WishList.objects.new_wishlist(user=request.user)
            new_obj = True
            request.session['wishlist_id'] = wishlist_obj.id
        return wishlist_obj, new_obj


    def new_wishlist(self, user=None):
        user_obj = None
        if user is not None:
            if user.is_authenticated:
                user_obj = user
        return self.model.objects.create(user=user_obj)



class WishList(models.Model):
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE)
    products = models.ManyToManyField(Product, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    # price = models.DecimalField(max_digits=10, decimal_places=2, default=00.00)
    
    objects = WishListManager()

    def __str__(self):
        return str(self.id)

    # def get_absolute_url(self):
    #     return reverse("WishList_detail", kwargs={"pk": self.pk})

