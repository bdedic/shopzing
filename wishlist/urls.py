from django.urls import path

from .views import wishlist_home,wishlist_update

urlpatterns = [
    path('', wishlist_home, name='home'),
    path('update/', wishlist_update, name='update'),
    
]
