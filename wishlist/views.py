from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic import ListView

from products.models import Product, ProductImage

from .models import WishList
from django.utils import timezone
from datetime import timedelta


# class WishListView(ListView):
#     model = WishList
#     template_name = "wishlist/wishlist_home.html"

#     def create(self, request):
#         request = self.request
#         wishlist_obj = WishList.objects.new_or_get(request)

def wishlist_detail_api_view(request):
    wishlist_obj, new_obj = WishList.objects.new_or_get(request)
    products = [{"id": x.id, "url": x.get_absolute_url(), "title": x.title, "price": x.price} for x in wishlist_obj.products.all()]
    products1 = [Product.objects.filter(title__icontains=item) for item in wishlist_obj.products.all()]
    images = []
    for img in products1:
        x = ProductImage.objects.filter(product=img[0]).first()
        images.append({"image":str(x.image)})
    return JsonResponse({"products": products, "images":images})



def wishlist_home(request):
    if request.user.is_authenticated:
        wishlist_obj, new_obj = WishList.objects.new_or_get(request)
    #products = wishlist_obj.products.all()
        return render(request, "wishlist/wishlist_home.html", {"wishlist": wishlist_obj})
    return redirect("login")



def wishlist_update(request):
    product_id = request.POST.get('product_id')
    if product_id is not None:
        try:
            product_obj = Product.objects.get(id=product_id)
        except Product.DoesNotExist:
            return redirect("wishlist:home")
        wishlist_obj, new_obj = WishList.objects.new_or_get(request)
        if product_obj in wishlist_obj.products.all():
            wishlist_obj.products.remove(product_obj)
            added = False
        else:
            wishlist_obj.products.add(product_obj)
            added = True
        request.session['wishlist_items'] = wishlist_obj.products.count()
        # return redirect(product_obj.get_absolute_url())
        if request.is_ajax():
            json_data = {
                "added": added,
                "removed": not added,
                "wishlistItemCount": wishlist_obj.products.count(),
            }
            return JsonResponse(json_data)
    return redirect("wishlist:home")
