

from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import get_template
from django.utils import timezone

from celery.decorators import periodic_task
from celery.task.schedules import crontab
from products.models import Product
from wishlist.models import WishList
from notifications.models import Notification
import time


@periodic_task(
    run_every=(crontab(minute='*/2')),
    name="send_mail_notification",
    ignore_result=True
)
def send_mail_notification():
    global discounted_products, wish
    wishlists = WishList.objects.all()
    for wish in wishlists:
        notification_user = Notification.objects.get(user=wish.user)
        if notification_user.enabled == True:
            all_products = wish.products.all()
            discounted_products = []
            for product_ in all_products:
                product_obj = Product.objects.get(title=product_.title)
                today = timezone.now().date()
                prod_updated = product_obj.updated.date()
                difference = today - prod_updated
                if product_obj.discounted == True and difference.days == 0:
                    discounted_products.append([product_.title,
                        str(product_.price),
                        str(product_.discounted_price),
                        product_.shop])
            send_email()
            time.sleep(1)
        # else: return notification_user
    discounted_products.clear()

def send_email():
    base_url = getattr(settings, 'BASE_URL' , 'https://shopzing.herokuapp.com/')
    path = "{base}/wishlist/".format(base=base_url)#, path=key_path)
    context = {
        'path': path,
        'products': discounted_products,
    }
        
    txt_ = get_template('notifications/emails/notification_email.txt').render(context)
    html_ = get_template('notifications/emails/notification_email.html').render(context)
    subject = 'An item from your wishlist is on discount'
    from_email = settings.DEFAULT_FROM_EMAIL
    recipient_list = [str(wish.user)]
    sent_mail = send_mail(subject, txt_, from_email, recipient_list, html_message=html_, fail_silently=False)
    return sent_mail
    
