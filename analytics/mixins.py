from .signals import object_viewed_signal
from django.utils.text import slugify
from products.models import Product


class ObjectViewedMixin(object):
    def get_context_data(self, **kwargs):
        context = super(ObjectViewedMixin, self).get_context_data(**kwargs)
        request = self.request
        query = self.kwargs.get('slug')
        query = slugify(query)
        instance = Product.objects.detail(query)
        instance_obj = instance.first()
        if instance:
            object_viewed_signal.send(instance_obj.__class__, instance=instance_obj, request=request)
        return context
    