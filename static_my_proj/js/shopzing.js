$(document).ready(function(){


  $('.fliper-btn').on('click', function() {
    $(this).closest('.card').toggleClass('flipped');
  });


  //Contact form

    var contactForm = $(".contact-form")
    var contactFormMethod = contactForm.attr("method")
    var contactFormEndPoint = contactForm.attr("action")
    
    function displaySubmit(submitBtn, defaultText, doSubmit){
      if (doSubmit){
        submitBtn.addClass("disabled")
        submitBtn.html("<i class='fa fa-spin fa-spinner'></i> Sending...")
      }
      else{
        submitBtn.removeClass("disabled")
        submitBtn.html(defaultText)
      }
    }


    contactForm.submit(function(event){
      event.preventDefault()
      var contactFormSubmitBtn = contactForm.find("[type='submit']")
      var contactFormSubmitBtnTxt = contactFormSubmitBtn.text()
      var contactFormData = contactForm.serialize()
      var thisForm = $(this)
      displaySubmit(contactFormSubmitBtn, "", true)
      $.ajax({
        url: contactFormEndPoint,
        method: contactFormMethod,
        data: contactFormData,
        success: function(data){
          thisForm[0].reset()
          $.alert({
            title: "Success!",
            content: data.message,
            theme: "modern"
          })
          setTimeout(function(){
            displaySubmit(contactFormSubmitBtn, contactFormSubmitBtnTxt, false)
          }, 500)
        },
        error: function(error){
          var jsonData = error.responseJSON
          var msg = ""

          $.each(jsonData, function(key, value){
            msg += key + ": " + value[0].message + "</br>"
          })
          $.alert({
            title: "Oops!",
            content: msg,
            theme: "modern"
          })
          setTimeout(function(){
            displaySubmit(contactFormSubmitBtn, contactFormSubmitBtnTxt, false)
          }, 500)
        }
      })
    })


    //Auto search

    var searchForm = $(".search-form")
    var searchInput = searchForm.find("[name='q']")
    var typingTimer;
    var typingInterval = 500
    var searchBtn = searchForm.find("[type='submit']")

    searchInput.keyup(function(event){
      clearTimeout(typingTimer)
      typingTimer = setTimeout(performSearch, typingInterval)
    })
    searchInput.keydown(function(event){
      clearTimeout(typingTimer)
    })

    function displaySearching(){
      searchBtn.addClass("disabled")
      searchBtn.html("<i class='fa fa-spin fa-spinner'></i> Searching...")
    }

    function performSearch(){
      displaySearching()
      var query = searchInput.val()
      setTimeout(function(){
        window.location.href='/search/?q=' + query
      }, 1000)
      
    }


    //Wishlist + Add products
    var productForm = $(".form-product-ajax")
    productForm.submit(function(event){
      event.preventDefault();
      var thisForm = $(this)
      // var actionEndpoint = thisForm.attr("action");
      var actionEndpoint = thisForm.attr("data-endpoint");
      var httpMethod = thisForm.attr("method");
      var formData = thisForm.serialize();
      

      $.ajax({
        url: actionEndpoint,
        method: httpMethod,
        data: formData,
        

        success: function(data){
          var submitSpan = thisForm.find(".submit-span")
          if (data.added){
            submitSpan.html('In wishlist <button type="submit" class="btn btn-link">Remove?</button>')
          }
          else {
            submitSpan.html('<button class="btn btn-success">Add to wishlist</button>')
          }
          var wishlistCount = $(".navbar-wish-count")
          wishlistCount.text(data.wishlistItemCount)
          var currentPath = window.location.href
          if (currentPath.indexOf("wishlist") != -1){
            updateWishlist()
          }
        },
        error: function(errorData){
          $.alert({
            title: "Oops!",
            content: "An error has occured",
            theme: "modern"
          })
        }
      })
    })

    function updateWishlist(){
      var wishlistTable = $(".wishlist-home")
      var wishlistBody = wishlistTable.find(".wishlist-body")
      var productRows = wishlistBody.find(".wishlist-product")
      
      var currentUrl = window.location.href


      var updateWishlistUrl = '/api/wishlist/';
      var updateWishlistMethod = "GET";
      var data = {};

      
      

      $.ajax({
        url: updateWishlistUrl,
        method: updateWishlistMethod,
        data: data,
        success: function(data){ 
          var hiddenWishlistItemRemoveForm = $(".wishlist-item-remove-form")
          if (data.products.length > 0){
            productRows.html(" ")
            i = data.products.length
            
            $.each(data.products, function(index, value){ 
              var newWishlistItemRemove = hiddenWishlistItemRemoveForm.clone()
              newWishlistItemRemove.css("display", "block")
              newWishlistItemRemove.find(".wishlist-item-product-id").val(value.id)
              
              wishlistBody.prepend("<tr class='wishlist-product'><th scope=\"row\" class='tb-counter'>" + i + "</th><td class='tb-img'><img class='src-img' src="+ "/media/" + data.images[index].image + " " + "width='32%' height='30%'></td><td class='tb-title'><a href='" + value.url + "'><h6>" + value.title + "</h6></a>"
              + newWishlistItemRemove.html() + "</td><td class='tb-price'>" + value.price + "</td></tr>")
              
              i --
              
            })
            
          }
          
          else {
            window.location.href = currentUrl
            
          }
        },
        error: function(errorData){
          $.alert({
            title: "Oops!",
            content: "An error has occured",
            theme: "modern"
          })
        }
      })
    }


    //Check/uncheck radio button


    // var grd = function(){
    //   $("input[type='radio']").click(function() {
    //     var previousValue = $(this).attr('previousValue');
    //     var name = $(this).attr('name');
    
    //     if (previousValue == 'checked') {
    //       $(this).removeAttr('checked');
    //       $(this).attr('previousValue', false);
          
    //     } else {
    //       $("input[name="+name+"]:radio").attr('previousValue', false);
    //       $(this).attr('previousValue', 'checked');
    //     }
    //   });
    // };

    // grd('state');

  })