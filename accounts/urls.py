from django.urls import path

from .views import AccountHomeView,AccountEmailActivateView

urlpatterns = [
    path('', AccountHomeView.as_view(), name='home'),
    path('email/confirm/<key>/', AccountEmailActivateView.as_view(), name='email-activate'),
    path('email/resend-activation/', AccountEmailActivateView.as_view(), name='resend-activation'),
]
