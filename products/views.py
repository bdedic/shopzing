from .models import Product,ProductImage
from tags.models import Tag
from django.http import Http404
from django.shortcuts import get_object_or_404, render
from django.views.generic import DetailView, ListView
from django.utils.text import slugify

from wishlist.models import WishList
from analytics.mixins import ObjectViewedMixin
# from analytics.signals import object_viewed_signal
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.utils import timezone
from datetime import timedelta



# class ProductDiscountedListView(ListView):
#     template_name = 'products/product_list.html'
   
#     def get_queryset(self, *args, **kwargs):
#         request = self.request
#         return Product.objects.all().discounted()


class ProductDiscountedDetailView(DetailView):
    template_name = 'products/product_detail_discounted.html'
    
    def get_queryset(self, *args, **kwargs):
        request = self.request
        return Product.objects.all().discounted()


class ProductListView(ListView):
    model = Product
    template_name = 'products/product_list.html'
    context_object_name = 'products'  # Default: object_list
    paginate_by = 8 
   


    def get_context_data(self, **kwargs):
        context = super(ProductListView, self).get_context_data(**kwargs)
        request = self.request
        if request.user.is_authenticated:
            wishlist_obj, new_obj = WishList.objects.new_or_get(request)
            context['wishlist'] = wishlist_obj
        return context

    def get_queryset(self, *args, **kwargs):
        queryset = super(ProductListView, self).get_queryset()
        queryset = Product.objects.all()
        paginator = Paginator(queryset, self.paginate_by)
        page = self.request.GET.get('page')
        request = self.request
        if request.user.is_authenticated:
            wishlist_obj, new_obj = WishList.objects.new_or_get(request)
            # wishlist_obj = WishList.objects.all().get(user=request.user)
            if request.user == wishlist_obj.user:
                request.session['wishlist_items'] = wishlist_obj.products.count()
        try:
            products = paginator.page(page)
        except PageNotAnInteger:
            products = paginator.page(1)
        except EmptyPage:
            products = paginator.page(paginator.num_pages)
        
        return queryset

    # def get_queryset(self, *args, **kwargs):
    #     request = self.request
    #     return Product.objects.all()

    # def get_context_data(self, **kwargs):
    #     context = super(ProductListView, self).get_context_data(**kwargs) 
    #     qs = Product.objects.all().to_display()
    #     paginator = Paginator(qs, self.paginate_by)

    #     page = self.request.GET.get('page')

    #     try:
    #         products = paginator.page(page)
    #     except PageNotAnInteger:
    #         products = paginator.page(1)
    #     except EmptyPage:
    #         products = paginator.page(paginator.num_pages)

    #     context['qs'] = products
    #     return context

    


class ProductDetailSlugView(ObjectViewedMixin,ListView):
    model = Product
    template_name = 'products/product_detail.html'
    
    #queryset = Product.objects.filter(slug__icontains=slug)
    #queryset = ProductTag.objects.all()
    #print(queryset)

    # def get_object(self, *args, **kwargs):
    #     request = self.request
    #     slug = self.kwargs.get('slug')
    #     # queryset = Product.objects.filter(slug__icontains=slug)
    #     instance = get_object_or_404(Product, slug__icontains=slug)
    #     return instance

    
    # def get_context_data(self, **kwargs):
    #     context = super(ProductDetailSlugView, self).get_context_data(**kwargs)
    #     slug = self.kwargs.get('slug')
    #     # # query = Product.objects.filter(slug__icontains=slug)
    #     context['query'] = Product.objects.filter(slug__icontains=slug)
    #     #     print(Product.objects.filter(slug__icontains=slug))
    #     # return context
    #     return context
    # def get_queryset(self, *args, **kwargs):
    #     request = self.request
    #     #slug = self.kwargs.get('slug')
    #     query = self.kwargs.get('slug')
    #     context = Product.objects.filter(title__icontains=query)
    #     # context['image'] = ProductImage.objects.all()
    #     return context

    def get_context_data(self, **kwargs):
        context = super(ProductDetailSlugView, self).get_context_data(**kwargs)
        request = self.request
        if request.user.is_authenticated: 
            wishlist_obj, new_obj = WishList.objects.new_or_get(request)
            context['wishlist'] = wishlist_obj
            return context
        else:
            query = self.kwargs.get('slug')
            query = slugify(query)
            instance = Product.objects.detail(query)
            instance_obj = instance.first()
            context['wishlist'] = instance
            return context

    def get_queryset(self, *args, **kwargs):
        request = self.request
        query = self.kwargs.get('slug')
        query = slugify(query)
        instance = Product.objects.detail(query)
        instance_obj = instance.first()
        return instance
    


   

class ProductDetailView(DetailView):
    #model = Product
    template_name = 'products/product_detail.html'
   
    def get_context_data(self, **kwargs):
        context = super(ProductDetailView,self).get_context_data(**kwargs)
        return context

    def get_object(self, *args, **kwargs):
        request = self.request
        pk = self.kwargs.get('pk')
        instance = Product.objects.get_by_id(pk)
        if instance is None:
            raise Http404("Product does not exist!")
        return instance
    
    

class NewProductsListView(ListView):
    template_name = 'products/new_product_list.html'
    
    def get_queryset(self, *args, **kwargs):
        now = timezone.now() + timedelta(days=1)
        last_seven = now - timedelta(days=7)
        created = Product.objects.filter(timestamp__range=[last_seven.date(), now.date()]).active()
        return created
    