import os
from django.db import models
from .utils import unique_slug_generator
from django.db.models.signals import pre_save, post_save
from django.urls import reverse
from django.db.models import Q






class ProductQuerySet(models.QuerySet):
    def discounted(self):
        return self.filter(discounted=True, active=True)

    # def new(self):
    #     now = timezone.now()
    #     created = self.objects.all()
    #     print(created)
    #     result = created - now
    #     if result.days <= -7:
    #         return self.filter(active=True)
    #     return self.filter(discounted=True, active=True)

    def active(self):
        return self.filter(active=True)

    def search(self, query):
        lookups = (Q(title__icontains=query) |
          Q(weight__icontains=query) |
          Q(tag__title__icontains=query))
        return self.filter(lookups).distinct()

    def detail(self, query):
        return self.filter(Q(slug__icontains=query))

    def to_display(self):
        return self.filter(to_display=True)

    





class ProductManager(models.Manager):
    def get_queryset(self):
        return ProductQuerySet(self.model, using=self._db)

    def all(self):
        return self.get_queryset().active()


    def discounted(self):
        return self.get_queryset().discounted()


    def get_by_id(self, id):
        qs = self.get_queryset().filter(id=id)
        if qs.count() == 1:
            return qs.first()
        return None

    def search(self, query):
        return self.get_queryset().to_display().search(query)

    def detail(self, query):
        return self.get_queryset().detail(query)

    




def get_filename_ext(filepath):
    base_name = os.path.basename(filepath)
    name, ext = os.path.splitext(base_name)
    return name, ext


def upload_image_path(instance, filename):
    name, ext = get_filename_ext(filename)
    return 'products/{shop}/{name}{ext}'.format(shop=instance.shop, name=name, ext=ext)


class Product(models.Model):
    title = models.CharField(max_length=50)
    slug = models.SlugField(blank=True, unique=True)
    weight = models.CharField(max_length=50, null=True)
    description = models.TextField(blank=True)
    shop = models.CharField(max_length=50, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=00.00)
    discounted_price = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    discounted_until = models.DateTimeField(blank=True, null=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)
    to_display = models.BooleanField(default=False)
    discounted = models.BooleanField(default=False)
    brand_logo = models.ImageField(upload_to=upload_image_path ,blank=True, null=True, editable=True)

    class Meta:
       ordering = ['id']
    
    objects = ProductManager()

    # def first_image(self):
    #     # code to determine which image to show. The First in this case.
    #     return self.images[0]

    def get_absolute_url(self):
        return reverse('products:detail', kwargs={'slug': self.title})
        # return '/products/{slug}/'.format(slug=self.slug)

    def __str__(self):
        return self.title

def product_pre_save_reciever(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(product_pre_save_reciever, sender=Product)


def product_title_capitalize_pre_save_reciever(sender, instance, *args, **kwargs):
    instance.title = instance.title.upper()

pre_save.connect(product_title_capitalize_pre_save_reciever, sender=Product)



def get_filename_ext(filepath):
    base_name = os.path.basename(filepath)
    name, ext = os.path.splitext(base_name)
    return name, ext


def upload_image_path(instance, filename):
    name, ext = get_filename_ext(filename)
    return 'products/{shop}/{name}{ext}'.format(shop=instance.shop, name=name, ext=ext)


class ProductImage(models.Model):
    product = models.ForeignKey(Product , on_delete=models.CASCADE)
    image = models.ImageField(upload_to=upload_image_path ,blank=True, null=True, editable=True)
    # image_height = models.PositiveIntegerField(null=True, blank=True, editable=False, default="100")
    # image_width = models.PositiveIntegerField(null=True, blank=True, editable=False, default="100")


    @property
    def shop(self):
        return self.product.shop

    # def get_image(self):
    #     return self.image.first()

