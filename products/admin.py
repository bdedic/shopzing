from django.contrib import admin

from .models import Product, ProductImage


class ProductImageInline(admin.TabularInline):
    model = ProductImage
    extra = 3

class ProductAdmin(admin.ModelAdmin):
    inlines = [ ProductImageInline, ]
    list_display = ['__str__', 'slug']
    class Meta:
        model = Product



admin.site.register(Product,ProductAdmin)
