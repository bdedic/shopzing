from django.urls import path

from .views import ProductListView, ProductDetailSlugView, NewProductsListView#, ProductDiscountedListView, ProductDiscountedDetailView,

urlpatterns = [
    # path('discounted/', ProductDiscountedListView.as_view(), name='discounted'),
    # path('discounted/<pk>/', ProductDiscountedDetailView.as_view(), name='detail_discounted'),
    path('products/new/', NewProductsListView.as_view(), name='new_products'),
    path('products/', ProductListView.as_view(), name='product_list'),
    # path('products/<pk>/', ProductDetailView.as_view(), name='details'),
    path('products/<slug>/', ProductDetailSlugView.as_view(), name='detail'),
    
]
