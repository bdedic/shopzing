from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from products.models import Product
from wishlist.models import WishList



# Create your models here.


User = settings.AUTH_USER_MODEL

class NotificationManager(models.Manager):
    def get_notifications(self, request):
        qs = self.get_queryset().filter(user=request.user)
        if qs.count() == 1:
            new_notification_obj = False
            notification_obj = qs.first()
            if request.user.is_authenticated and notification_obj.user is None:
                notification_obj.user = request.user
                notification_obj.save()
        else:
            notification_obj = Notification.objects.new_notification(user=request.user)
            new_notification_obj = True
            request.session['notification_id'] = notification_obj.id
        return notification_obj, new_notification_obj

    
    def new_notification(self, user=None):
        user_obj = None
        if user is not None:
            if user.is_authenticated:
                user_obj = user
        return self.model.objects.create(user=user_obj)
        



class Notification(models.Model):
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE)
    enabled = models.BooleanField(default=False)
    enabled_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    


    objects = NotificationManager()

    def __str__(self):
        return str(self.user)

    # def send_notification_email(self, user=None):
    #     user_ = user
    #     wishlist_obj = WishList.objects.get(user=user_)

