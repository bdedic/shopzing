from django.contrib import admin
from .models import Notification

# Register your models here.


class NotificationAdmin(admin.ModelAdmin):
    readonly_fields = ('enabled_date',)


admin.site.register(Notification, NotificationAdmin)