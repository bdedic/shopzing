from django.shortcuts import redirect, render

from .models import Notification

# class NotificationView(View):
#     def get(self, request, *args, **kwargs):
#         user = request.user
        
def notification_home(request):
    if request.user.is_authenticated:
        notification_obj = Notification.objects.get_notifications(request)
        user = request.user
        obj = Notification.objects.get(user=user)
        context = {
        'user' : obj.user,
        'enabled' : obj.enabled,
        'enabled_date' : obj.enabled_date
        }
        return render(request, 'notifications/notifications_home.html', context)
    else:
        return redirect("login")





def notification_update(request):
    if request.user.is_authenticated:
        user = request.user
        obj = Notification.objects.get(user=user)
        if obj.enabled == True:
            obj.enabled = False
        else:
            obj.enabled = True
    # obj.save(update_fields=['enabled'])
    obj.save()
    return redirect("notifications:home")

# def notification_enabled(request):
#     next_ = request.GET.get('next')
#     next_post = request.POST.get('next')
#     redirect_path = next_ or next_post or None
#     if request.method == 'POST':
#         print(request.POST)

#         if is_safe_url(redirect_path, request.get_host()):
#             return redirect(redirect_path)
#         else:
#             return redirect("notifications:home")
