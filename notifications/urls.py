from django.urls import path

from .views import notification_home, notification_update

urlpatterns = [
    path('', notification_home, name='home'),
    path('update/', notification_update, name='update'),
    
]